Catalyst Strategies
===================

Prerequisites
-------------

- Anaconda Python environment
- Python 3
- TA-Lib


Initial Setup
-------------

- Create the catalyst environment with anaconda: `conda env create -f python3.6-environment.yml`
- Run `conda activate catalyst` to activate the python environment
- Install TA-Lib: `pip install TA-Lib`


Backtesting
-----------

1. ingest data: `catalyst ingest-exchange -x <exchange> -i <coin_pair> -f <minute|daily>` (defaults to past year)
2. `./run_strat.sh`
