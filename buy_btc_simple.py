"""This is a simple strategy that orders 1 bitcoin every iteration"""

import matplotlib.pyplot as plt
from catalyst.api import order, record, symbol


def initialize(context):
    """called exactly once when our algorithm starts running

    context - an augmented Python dictionary used for maintaining state
        throughout the simulation process, and can be referenced in different
        parts of our algorithm. Any variables that we want to persist between
        function calls should be stored in context instead of using global
        variables. Context variables can be accessed and initialized using dot
        notation (context.some_attribute)
    """
    context.asset = symbol('btc_usdt')


def handle_data(context, data):
    """called once per iteration

    context - an augmented Python dictionary used for maintaining state
        throughout the simulation process, and can be referenced in different
        parts of our algorithm. Any variables that we want to persist between
        function calls should be stored in context instead of using global
        variables. Context variables can be accessed and initialized using dot
        notation (context.some_attribute)
    data - an object that stores several API functions that allow us to look up
        current or historical pricing and volume data for any asset
    """
    order(context.asset, 1)
    record(btc=data.current(context.asset, 'price'))


def analyze(context, perf):
    """called once at the end of the algorithm

    context - an augmented Python dictionary used for maintaining state
        throughout the simulation process, and can be referenced in different
        parts of our algorithm. Any variables that we want to persist between
        function calls should be stored in context instead of using global
        variables. Context variables can be accessed and initialized using dot
        notation (context.some_attribute)
    perf - a pandas dataframe containing the performance data for our algorithm
    """
    ax1 = plt.subplot(211)
    perf.portfolio_value.plot(ax=ax1)
    ax1.set_ylabel('portfolio value')
    ax2 = plt.subplot(212, sharex=ax1)
    perf.btc.plot(ax=ax2)
    ax2.set_ylabel('bitcoin price')
    plt.show()
