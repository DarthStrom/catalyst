"""This strategy buys when the macd histogram is negative with a positive slope
and sells when the macd histogram is positive with a negative slope
"""

import talib
import matplotlib.pyplot as plt
import numpy as np
import catalyst.api as api

LONG_WINDOW = 35
N_WINDOW = 20


def initialize(context):
    """called exactly once when our algorithm starts running

    context - an augmented Python dictionary used for maintaining state
        throughout the simulation process, and can be referenced in different
        parts of our algorithm. Any variables that we want to persist between
        function calls should be stored in context instead of using global
        variables. Context variables can be accessed and initialized using dot
        notation (context.some_attribute)
    """
    context.i = 0
    context.asset = api.symbol('btc_usdt')


def handle_data(context, data):
    """called once per data tick

    context - an augmented Python dictionary used for maintaining state
        throughout the simulation process, and can be referenced in different
        parts of our algorithm. Any variables that we want to persist between
        function calls should be stored in context instead of using global
        variables. Context variables can be accessed and initialized using dot
        notation (context.some_attribute)
    data - an object that stores several API functions that allow us to look up
        current or historical pricing and volume data for any asset
    """
    if should_skip_ahead(context, LONG_WINDOW):
        return

    prices = data.history(
        context.asset, fields='close', bar_count=LONG_WINDOW, frequency='1T')
    _macd, _macd_signal, macd_histogram = talib.MACD(prices)
    last2_macd_histogram = macd_histogram.tail(2)
    prev_macd_histogram_value = last2_macd_histogram.iat[0]
    last_macd_histogram_value = last2_macd_histogram.iat[1]
    histogram_slope = last_macd_histogram_value - prev_macd_histogram_value

    if last_macd_histogram_value > 0 and histogram_slope < 0:
        api.order_target_percent(context.asset, 0)
    elif last_macd_histogram_value < 0 and histogram_slope > 0:
        api.order_target_percent(context.asset, 1)


def analyze(context, perf):
    """called once at the end of the algorithm

    context - an augmented Python dictionary used for maintaining state
        throughout the simulation process, and can be referenced in different
        parts of our algorithm. Any variables that we want to persist between
        function calls should be stored in context instead of using global
        variables. Context variables can be accessed and initialized using dot
        notation (context.some_attribute)
    perf - a pandas dataframe containing the performance data for our algorithm
    """
    exchange = list(context.exchanges.values())[0]
    quote_currency = exchange.quote_currency.upper()

    ax1 = plt.subplot(111)
    perf.loc[:, ['portfolio_value']].plot(ax=ax1)
    ax1.legend_.remove()
    ax1.set_ylabel('Portfolio Value\n({})'.format(quote_currency))
    start, end = ax1.get_ylim()
    ax1.yaxis.set_ticks(np.arange(start, end, (end - start) / 5))

    plt.show()


def should_skip_ahead(context, window):
    """Advance to the next iteration if not enough data for the window"""
    context.i += 1
    return context.i < window
