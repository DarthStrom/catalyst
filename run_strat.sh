#!/usr/bin/env bash

strategy=$1

# available data to ingest:
# https://enigma.co/catalyst/status/

# btc_usdt data starting 2017-08-18
#           daily ending 2018-10-06
#          minute ending 2018-10-07

catalyst run -f ${strategy}.py \
    -c usd \
    -x binance \
    -s 2018-01-07 -e 2018-1-07 \
    -o ${strategy}.pickle \
    --capital-base 10000 \
    --data-frequency minute
