"""This strategy attempts to emulate the rules defined by turtle traders
experiment run by Richard Dennis and William Eckhardt
"""

import matplotlib.pyplot as plt
import numpy as np
import catalyst.api as api

MAX_UNITS_FOR_SINGLE_MARKET = 4
MAX_UNITS_IN_CLOSELY_CORRELATED = 6
MAX_UNITS_IN_LOOSELY_CORRELATED = 10
MAX_UNITS_IN_SINGLE_DIRECTION = 12

BREAKOUT_WINDOW = 55
EXIT_WINDOW = 20
N_WINDOW = 20


def initialize(context):
    """called exactly once when our algorithm starts running

    context - an augmented Python dictionary used for maintaining state
        throughout the simulation process, and can be referenced in different
        parts of our algorithm. Any variables that we want to persist between
        function calls should be stored in context instead of using global
        variables. Context variables can be accessed and initialized using dot
        notation (context.some_attribute)
    """
    context.i = 0
    context.asset = api.symbol('btc_usdt')
    context.atr = None
    context.units = 0
    context.last_unit = None
    context.stop_price = None


def handle_data(context, data):
    """called once per data tick

    context - an augmented Python dictionary used for maintaining state
        throughout the simulation process, and can be referenced in different
        parts of our algorithm. Any variables that we want to persist between
        function calls should be stored in context instead of using global
        variables. Context variables can be accessed and initialized using dot
        notation (context.some_attribute)
    data - an object that stores several API functions that allow us to look up
        current or historical pricing and volume data for any asset
    """
    max_window = max(
        BREAKOUT_WINDOW,
        EXIT_WINDOW,
        N_WINDOW,
    )

    if should_skip_ahead(context, max_window):
        return

    compute_atr(context, data, N_WINDOW)

    breakout_high = previous_high(context, data, BREAKOUT_WINDOW)

    price = get_price(context, data)

    api.record(
        price=price,
        cash=context.portfolio.cash,
        breakout_high=breakout_high,
        atr=context.atr,
    )

    if should_skip(context, data):
        return

    if context.units == 0:
        # entry
        if price >= breakout_high:
            add_unit(context, data)
    else:
        # stop
        if price < context.stop_price:
            sell_all(context)
        # exit
        elif price < previous_low(context, data, EXIT_WINDOW):
            sell_all(context)
        # add to position
        elif price > (context.last_unit + 0.5 * context.atr
                      ) and context.units < MAX_UNITS_FOR_SINGLE_MARKET:
            add_unit(context, data)


def analyze(context, perf):
    """called once at the end of the algorithm

    context - an augmented Python dictionary used for maintaining state
        throughout the simulation process, and can be referenced in different
        parts of our algorithm. Any variables that we want to persist between
        function calls should be stored in context instead of using global
        variables. Context variables can be accessed and initialized using dot
        notation (context.some_attribute)
    perf - a pandas dataframe containing the performance data for our algorithm
    """
    exchange = list(context.exchanges.values())[0]
    quote_currency = exchange.quote_currency.upper()

    ax1 = plt.subplot(111)
    perf.loc[:, ['portfolio_value']].plot(ax=ax1)
    ax1.legend_.remove()
    ax1.set_ylabel('Portfolio Value\n({})'.format(quote_currency))
    start, end = ax1.get_ylim()
    ax1.yaxis.set_ticks(np.arange(start, end, (end - start) / 5))

    plt.show()


def sell_all(context):
    """Liquidate all assets"""
    api.order_target_percent(context.asset, 0)
    context.units = 0
    context.last_unit = None
    context.stop_price = None


def get_price(context, data):
    """Find the current price for the asset in the context"""
    price = data.current(context.asset, 'price')
    return price


def add_unit(context, data):
    """Order one turtles 'unit' of the asset in the context"""
    api.order(
        context.asset,
        unit_size(context),
    )
    price = get_price(context, data)
    # todo: handle the case where the stop would exceed first purchase price
    # todo: look into whipsaw alternative
    context.stop_price = price - 2 * context.atr
    context.units += 1

    # todo: use the actual fill price to account for slippage
    context.last_unit = price


def should_skip_ahead(context, window):
    """Advance to the next iteration if not enough data for the window"""
    context.i += 1
    return context.i < window


def breakout_data(context, data, window):
    """Get the historical prices for the window"""
    return data.history(
        context.asset, 'price', bar_count=window, frequency="1T")


def previous_high(context, data, window):
    """Get the high price from the data in a window"""
    datas = breakout_data(context, data, window)
    return datas.max()


def previous_low(context, data, window):
    """Get the low price from the data in a window"""
    return breakout_data(context, data, window).min()


def should_skip(context, data):
    """Don't open a new order if you already have one"""
    # maybe we don't want to do this?
    orders = context.blotter.open_orders
    if len(orders) > 0:
        return True

    if not data.can_trade(context.asset):
        return True

    return False


def compute_atr(context, data, window):
    """Calculate the average true range (ATR) for the window"""
    high = data.current(context.asset, 'high')
    low = data.current(context.asset, 'low')
    previous_close = data.history(
        context.asset, 'close', bar_count=1, frequency="1T")[0]
    true_range = max(high - low, high - previous_close, previous_close - low)

    if context.atr is None:
        lows = data.history(
            context.asset, 'low', bar_count=window, frequency="1T")
        highs = data.history(
            context.asset, 'high', bar_count=window, frequency="1T")
        closes = data.history(
            context.asset, 'close', bar_count=window, frequency="1T")
        true_ranges = []
        for index, low in enumerate(lows):
            high = highs[index]
            if index > 0:
                previous = closes[index - 1]
            else:
                previous = (high + low) / 2
            true_ranges.append(
                max(high - low, high - previous, previous - low))
        context.atr = sum(true_ranges) / len(true_ranges)

    context.atr = (19 * context.atr + true_range) / 20
    return context.atr


def unit_size(context):
    """Get the number of asset in a unit"""
    account_size = context.portfolio.portfolio_value
    return (0.01 * account_size) / context.atr
